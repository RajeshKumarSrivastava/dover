package test

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/ateam/dover/constants"
)

//function provided by https://stackoverflow.com/questions/50120427/fail-unit-tests-if-coverage-is-below-certain-percentage

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	rc := m.Run()

	// rc 0 means we've passed,
	// and CoverMode will be non empty if run with -cover
	if rc == 0 && testing.CoverMode() != "" {
		c := testing.Coverage()
		if c < constants.TestCodeCoverageGatingThreshold {
			fmt.Printf("Tests passed but code coverage is too low, Threshold: %v", constants.TestCodeCoverageGatingThreshold)
			rc = -1
		}
	}
	os.Exit(rc)
}
