# Dover
A microservice to ferry files to GitLab and create Merge Requests for them.

# Getting started

*dover* is a service that listens to the requests from any services to creates merge requests against a GitLab repository - [manifests](https://gitlab.com/redhat/edge/ci-cd/manifests). The merge request proposes the changes to the package list file. [rome](https://gitlab.com/redhat/edge/ci-cd/a-team/rome) is one of the services that sends such a request to *dover*.
## API endpoints

### GET `/health` 
    RETURNS: 
        `{"alive": true}`

### POST `/submit/package_list`  
    PARAMS:
        {  
            "branch": "<branchname>",
            "commitMessage": "<commitmessage>",
            "data": "<data>",
            "fileName": "<fileName>"
        }

## Installation
### Using container

**Prerequisite**

- Install container - software packager. You can use any container of your choice ([docker](https://www.docker.com/) or [podman](https://podman.io/)) to build the service image and get it up and running. All the necessary rules are already defined in the *Makefile*. 
- Install RabbitMQ and start the service. Follow the [documentation](https://www.rabbitmq.com/download.html).

**To start *dover* in the development environment**

- Provide environment variables

```sh
export GITLAB_TOKEN=<gitlab access token with admin:repo privileges>

# ONLY SET THIS ENV VAR FOR TESTING
export DOVER_DRY_RUN=true
```

- Navigate to the project root directory and run `make image/run/dev`. This command will create the image called *dover* and start the container.

### Without container

#### Prerequisite

- Download and install [Go](https://golang.org/doc/install) version 1.16 or above
- Run the following

```sh
go mod download && go mod vendor
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o dover main.go
```

**To start *dover* in the development environment**

- Provide environment variables

```Bash
export GITLAB_TOKEN=<gitlab access token with admin:repo privileges>

# ONLY SET THIS ENV VAR FOR TESTING
export DOVER_DRY_RUN=true
```

- Navigate to project root directory and run `/usr/local/bin/dover`

## How to contribute

- Fork the project
- Clone your fork
- Add upstream remote: `git remote add upstream https://gitlab.com/redhat/edge/ci-cd/a-team/dover.git`
- Checkout new branch from *main* branch: `git checkout -b <branch-name>`
- Commit and push your branch

```Bash
git add <file-name>
git commit -m <commit-messages>
git push origin <branch-name>
```

- Create a merge request against the upstream repository and provide a summary of what it is about
- Assign reviewers
- Resolve reviews if any
- Wait for approval(s) and your branch will be merged

### Upstream / Downstream contributions

This repository has two branches used for deployment:

 ***main*** - The upstream branch, open for community contributions. Used for deployments to the `automotive-sig` repository.

 ***downstream*** - Limited in access for community contributions, merging requires maintainer permission/roles, used for downstream deployments.

The two deployment branches may be out of sync, pulling in these changes will require a toolchain developer to do so manually. It is also expected that the branches will have context-specific code, so please be cautious when rebasing/pulling changes across the ***main*** / ***downstream*** branches.

Please consider where you want your changes to land and in what priority, before submitting a merge request ask yourself the following questions:

 1) What deployment are my changes affecting? Am I running the main or downstream version?

 2) Are my changes context-specific? i.e. are they also applicable/fixing a problem in the other branch?
    - If so, please also make the maintainers aware of this, so that the issue can be fixed in both.

 3) Will my changes break existing deployments?

 4) Did I test my changes?

## Code guidelines

- Provide unit test(s)
- For code format, *isort*, *flake8* and *black* are applied.
- Before pushing your code, make sure that linter and unit tests pass.

```Bash
#Inside the project root directory, run the following commands:
make lint
make test
```

## Report issues

If you find any issue/bug, please don't hesitate to create one via this [link](https://gitlab.com/redhat/edge/ci-cd/a-team/dover/-/issues/new).
